﻿using System;

namespace LookingForArrayElements
{
    public static class DecimalCounter
    {
        /// <summary>
        /// Searches an array of decimals for elements that are in a specified range, and returns the number of occurrences of the elements that matches the range criteria.
        /// </summary>
        /// <param name="arrayToSearch">One-dimensional, zero-based <see cref="Array"/> of single-precision floating-point numbers.</param>
        /// <param name="ranges">One-dimensional, zero-based <see cref="Array"/> of range arrays.</param>
        /// <returns>The number of occurrences of the <see cref="Array"/> elements that match the range criteria.</returns>
        public static int GetDecimalsCount(decimal[]? arrayToSearch, decimal[]?[]? ranges)
        {
            int i, j = 0, res = 0;

            List<decimal> list = new List<decimal>();

            if (arrayToSearch is null || ranges is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch));
            }

            if (ranges.Length > 1 && (ranges[0] is null || ranges[1] is null))
            {
                throw new ArgumentNullException(nameof(ranges));
            }

            if (ranges.Length > 1 && ranges[0]!.Length != ranges[1]!.Length && ranges[0]!.Length != 0 && ranges[1]!.Length != 0)
            {
                throw new ArgumentException("Bad length of ranges");
            }

            if (arrayToSearch.Length == 0 || ranges.Length == 0)
            {
                return 0;
            }

            do
            {
                i = 0;

                do
                {
                    if (ranges[j]!.Length < 1)
                    {
                        break;
                    }

                    if (arrayToSearch[i] >= ranges[j]![0] && arrayToSearch[i] <= ranges[j]![1] && !list.Contains(arrayToSearch[i]))
                    {
                        list.Add(arrayToSearch[i]);
                        res++;
                    }

                    i++;
                }
                while (i < arrayToSearch.Length);

                j++;
            }
            while (j < ranges.Length);

            return res;
        }

        /// <summary>
        /// Searches an array of decimals for elements that are in a specified range, and returns the number of occurrences of the elements that matches the range criteria.
        /// </summary>
        /// <param name="arrayToSearch">One-dimensional, zero-based <see cref="Array"/> of single-precision floating-point numbers.</param>
        /// <param name="ranges">One-dimensional, zero-based <see cref="Array"/> of range arrays.</param>
        /// <param name="startIndex">The zero-based starting index of the search.</param>
        /// <param name="count">The number of elements in the section to search.</param>
        /// <returns>The number of occurrences of the <see cref="Array"/> elements that match the range criteria.</returns>
        public static int GetDecimalsCount(decimal[]? arrayToSearch, decimal[]?[]? ranges, int startIndex, int count)
        {
            int res = 0;

            List<decimal> list = new List<decimal>();

            if (startIndex < 0 || count < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex));
            }

            if (arrayToSearch is null || ranges is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch));
            }

            if (startIndex > arrayToSearch.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex));
            }

            if (ranges.Length > 1 && (ranges[0] is null || ranges[1] is null))
            {
                throw new ArgumentNullException(nameof(ranges));
            }

            if (ranges.Length > 1 && ranges[0]!.Length != ranges[1]!.Length && ranges[0]!.Length != 0 && ranges[1]!.Length != 0)
            {
                throw new ArgumentException("bad");
            }

            if (count > arrayToSearch.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(count));
            }

            if (arrayToSearch.Length == 0 || ranges.Length == 0)
            {
                return 0;
            }

            for (int j = 0; j < ranges.Length; j++)
            {
                if (ranges[j]!.Length < 1)
                {
                    break;
                }

                if (ranges[j] is null)
                {
                    throw new ArgumentNullException(nameof(ranges));
                }

                for (int i = startIndex; i < startIndex + count; i++)
                {
                    if (arrayToSearch[i] >= ranges[j]![0] && arrayToSearch[i] <= ranges[j]![1] && !list.Contains(arrayToSearch[i]))
                    {
                        list.Add(arrayToSearch[i]);
                        res++;
                    }
                }
            }

            return res;
        }
    }
}
